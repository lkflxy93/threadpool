#pragma once
#include "stddef.hpp"
class task_t
{
public:
	typedef void(task_fun_t)(void*);
	task_t( task_fun_t *fun_, void *arg_)
		:task_fun(fun_),
		arg(arg_)
	{

	}
	~task_t(void)
	{
	}
	void do_work()
	{
		if (task_fun)
		{
			task_fun(arg);
		}
	}
private:
	void *arg;
	task_fun_t *task_fun;
};
typedef std::shared_ptr<task_t> task_ptr_t;
